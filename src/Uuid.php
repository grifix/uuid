<?php

declare(strict_types=1);

namespace Grifix\Uuid;

use Grifix\Uuid\Exceptions\InvalidUuidException;
use Ramsey\Uuid\Uuid as RamseyUuid;

final class Uuid
{
    private function __construct(private readonly string $value)
    {
        $this->assertIsValid($value);
    }

    public static function createFromString(string $value): self
    {
        return new self($value);
    }

    public static function createRandom(): self
    {
        return new self((string)RamseyUuid::uuid4());
    }

    public function toString(): string
    {
        return $this->value;
    }

    public function isEqualTo(self $another): bool
    {
        return $this->value === $another->value;
    }

    private function assertIsValid(string $value): void
    {
        if (false === RamseyUuid::isValid($value)) {
            throw new InvalidUuidException($value);
        }
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
