<?php

declare(strict_types=1);

namespace Grifix\Uuid\Tests;

use Grifix\Uuid\Exceptions\InvalidUuidException;
use Grifix\Uuid\Uuid;
use PHPUnit\Framework\TestCase;

final class UuidTest extends TestCase
{
    public function testItCreatedRandom(): void
    {
        $uuid = Uuid::createRandom();
        self::assertInstanceOf(Uuid::class, $uuid);
    }

    public function testItCreatesFromString(): void
    {
        $uuid = Uuid::createFromString('31fd6f19-0e11-4ff6-8654-bb8e5ec5c5df');
        self::assertEquals('31fd6f19-0e11-4ff6-8654-bb8e5ec5c5df', $uuid->toString());
        self::assertEquals('31fd6f19-0e11-4ff6-8654-bb8e5ec5c5df', (string)$uuid);
    }

    public function testItCompares(): void
    {
        $uuidA = Uuid::createFromString('b8d488da-5b25-4097-bf10-91ad9040a8c4');
        $uuidB = Uuid::createFromString('b8d488da-5b25-4097-bf10-91ad9040a8c4');
        $uuidC = Uuid::createFromString('012ab456-53b4-446a-99e1-598b954c97c0');

        self::assertTrue($uuidA->isEqualTo($uuidB));
        self::assertFalse($uuidA->isEqualTo($uuidC));
    }

    public function testItDoesNotCreateInvalid(): void
    {
        $this->expectException(InvalidUuidException::class);
        $this->expectExceptionMessage('Uuid [invalid uuid] is invalid!');

        Uuid::createFromString('invalid uuid');
    }
}
